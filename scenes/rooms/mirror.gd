extends Node2D

onready var face = ["res://assets/mocks/captain0.png",
					"res://assets/mocks/captain3.png",
					"res://assets/mocks/captain6.png",
					]

onready var captain = $WallMock/ColorRect/ColorRect/Captain
#onready var label = $Vbox/Label
signal general_finished

func force_quit():
	emit_signal("general_finished",0)

func init():

	if Globals.per_week_estimation[-1][0] <3 :
		captain.set_texture(load(face[0]))
	elif Globals.per_week_estimation[-1][0] <5 : 
		captain.set_texture(load(face[1]))
	else:
		captain.set_texture(load(face[2]))

