extends KinematicBody2D


var clicked := false
signal reached

export(StreamTexture) var texture setget set_resource

var first_time :=true
func init():
	first_time=true
	show()


func set_resource(tex:StreamTexture):
	if not is_inside_tree():
		yield(self, "ready")
	$Bottle.set_texture(tex)

func _input_event(_viewport, event,_shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			clicked = true
		if event.button_index == BUTTON_LEFT and event.is_action_released("LeftClick"):
			clicked = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if clicked:
		var res=move_and_collide( get_global_mouse_position()-position)
		if res is KinematicCollision2D and res.collider is Node2D:
			if res.collider.is_in_group("Inner"):
				first_time = false
				emit_signal("reached")
				queue_free()
	else:
		var res=move_and_collide(Vector2(0,10))
		if res is KinematicCollision2D and res.collider is Node2D:
			if res.collider.is_in_group("Inner") && first_time:
				first_time = false
				emit_signal("reached")
				queue_free()
	if Input.is_action_just_released("LeftClick"):
		clicked = false

