extends Node2D

export (Array,int) var poi #shorthand for points of interest
export(StreamTexture) var texture setget set_resource

func set_resource(tex:StreamTexture):
	if not is_inside_tree():
		yield(self, "ready")
	$Deck.set_texture(tex)

func shade():
	$Deck.modulate = Color(55.0/255,52.0/255,61.0/255,1)
func light():
	$Deck.modulate = Color(1,1,1)
