enum action_t {go_to,stall,cont,cptn_display,bot_display,cptn_think}

enum positions {bed,poi1,three_dee_printer,poi2,tv,table,poi3,accurator,trademill,mirror}
const coords = [
	 [0,256],[700,256],[1380,256]
	,[0,512],[700,512],[1380,512]
	,[0,766],[700,766]
	]
var map_positions := {
	positions.keys()[positions.bed]:coords[0],
	positions.keys()[positions.poi1]:coords[1],
	positions.keys()[positions.three_dee_printer]:coords[2],
	positions.keys()[positions.poi2]:coords[3],
	positions.keys()[positions.tv]:coords[4],
	positions.keys()[positions.table]:coords[4],
	positions.keys()[positions.poi3]:coords[5],
	positions.keys()[positions.accurator]:coords[6],
	positions.keys()[positions.trademill]:coords[7],
	positions.keys()[positions.mirror]:coords[0],
	}

