extends Node

const parametrics = preload("parametrics.gd")
enum state{
	stop
	run
}
var current_state = state.run
var max_minute := parametrics.max_minute 
var max_second := parametrics.max_second
export (int) var minutes = max_minute
export (int) var seconds = max_second
signal finished
func get_seconds():
	return minutes * 60  + seconds
var last_second = OS.get_time().second
# Called every frame. 'delta' is the elapsed time since the previous frame.
func decrease_remaining_time():
	if(seconds == 0):
		seconds=60
		minutes=minutes-1
	if(minutes == -1):
		current_state = state.stop
	seconds = seconds -1
	if(minutes == 0  && seconds == 0):
		current_state = state.stop #fake trigger
		emit_signal("finished")
func restart(seconds_tmp):
	max_minute = seconds_tmp / 60
	max_second = seconds_tmp % 60
	minutes = max_minute
	seconds = max_second
	current_state = state.run


func pause():
	current_state = state.stop


func start():
	current_state = state.run


func _process(_delta):
	var now = OS.get_time()
	if (current_state == state.run && now.second !=last_second):
		decrease_remaining_time()
		last_second = now.second

