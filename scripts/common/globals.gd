extends Node

const enums = preload("enums.gd")

var current_poi = enums.positions.bed

var energy = 51 
var metal  = 60
var bar2   = 40
var bar3   = 47
var bar4   = 10
var bar5   = 80

var accurator_bought := false
var gym_bought := false

# Madrs page persistancy
var current_page :=  0
var selection_per_page := [0,0,0,0,0,0,0,0,0,0,0]

var per_week_user_estimation = []
onready var per_week_estimation = [[3,3,3,3,3,3,3,3,3,3]]

var day := 0

var click_misses :=0
var click_hits :=0

func save_user_profiling_data():
	return {
			"per_week_user_estimation":per_week_user_estimation,
			"per_week_estimation":per_week_estimation,
			"click_hits":click_hits,
			"click_misses":click_misses,
			"energy":energy,
			"metal":metal,
			"bar2":bar2,
			"bar3":bar3,
			"bar4":bar4,   
			"bar5":bar5 
		   }


func save_state():
	return {
			"day":day ,
			"energy":energy,
			"metal":metal,
			"current_poi": current_poi,
			"accurator_bought":accurator_bought,
			"gym_bought":gym_bought,
			}


func update_estimations():
	per_week_user_estimation.push_back(selection_per_page.slice(1,selection_per_page.size()))


func load_state():
	var file = File.new()
	file.open("user://save_state",File.READ)
	var text = file.get_as_text()
	file.close()
	var data = parse_json(text)
	print("Loading day:",data)
	day = data.day
	energy = data.energy
	metal  = data.metal
	current_poi = data.current_poi
	accurator_bought = data.accurator_bought

	gym_bought = data.gym_bought


func load_user_profiling():
	var file = File.new()
	file.open("user://save_user_profiling",File.READ)
	var text = file.get_as_text()
	file.close()
	var data = parse_json(text)
	print("Loading day:",data)
	per_week_user_estimation = data.per_week_user_estimation
	per_week_estimation = data.per_week_estimation

func init():
	var rng = RandomNumberGenerator.new() 
	rng.randomize()
	energy =rng.randi_range(20,60)
	metal  =rng.randi_range(0,50)
	bar2   =rng.randi_range(0,50)
	bar3   =rng.randi_range(0,50)
	bar4   =rng.randi_range(0,50)
	bar5   =rng.randi_range(0,50)
	per_week_estimation = [[rng.randi_range(1,6),
							rng.randi_range(1,6),
							rng.randi_range(1,6),
							rng.randi_range(1,6),
							rng.randi_range(1,6),
							rng.randi_range(1,6),
							rng.randi_range(1,6),
							rng.randi_range(1,6),
							rng.randi_range(1,6),
							rng.randi_range(1,3)]]

func _ready():
	add_to_group("Persistant")
	add_to_group("UserProfiling")
