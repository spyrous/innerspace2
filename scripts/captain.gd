extends Navigation2D
const enums = preload("common/enums.gd")
export(float) var character_speed = 150.0
var path = []
enum state_t {displays,thinks,walks,walks_while_waiting_to_display,walks_while_waiting_to_think}


onready var state = state_t.walks
onready var character = $CaptainSprite
onready var dialog = $CaptainSprite/Dialog
onready var cloud = $CaptainSprite/Cloud

signal finished_a_move 
signal dialog_clicked
signal dialog_missed
#signal cloud_clicked
#signal cloud_missed
signal sleeping

var delay_say_timer := Timer.new()

func disconnect_signals():
	var connections = get_signal_connection_list("finished_a_move")
	for conn in connections:
		disconnect(conn.signal, conn.target, conn.method)


func sleep():
	disconnect_signals()

	match state:
		state_t.walks_while_waiting_to_think:
			delay_say_timer.stop()
			delay_say_timer.disconnect("timeout",self,"think_delayed")
			pass
		state_t.walks_while_waiting_to_display:
			delay_say_timer.stop()
			delay_say_timer.disconnect("timeout",self,"say_delayed")
			pass
		state_t.displays:
			dialog.disconnect("clicked",self,"dialog_clicked")
			dialog.disconnect("not_clicked",self,"dialog_not_clicked")
			dialog.hide()
			pass
		state_t.thinks:
			cloud.disconnect("clicked",self,"cloud_clicked")
			cloud.disconnect("not_clicked",self,"cloud_not_clicked")
			cloud.hide()
			pass
		state_t.walks:
			pass
		state_t.stands:
			pass
	move([0,256])
	gain_speed()
	if connect("finished_a_move",self,"express_sleep",[],CONNECT_ONESHOT) != OK:
		push_warning("Couldn't connect captain finished_move_with sleep")

func express_sleep():
	$CaptainSprite.set_rotation_degrees(-90)
	emit_signal("sleeping")

func wake_up():
	$CaptainSprite.set_rotation_degrees(0)


func move(coords):
	state = state_t.walks
	_update_navigation_path(character.position,Vector2(coords[0],coords[1]))

func _process(delta):
	var walk_distance = character_speed * delta
	if walk_distance != 0:
		move_along_path(walk_distance)
	else :
		print("Division by zero prevented,character_speed:",character_speed,"Delta:",delta)


var current_string :=""
func say_delayed():
	state = state_t.displays
	#stop()
	dialog.connect("clicked",self,"dialog_clicked",[],CONNECT_ONESHOT)
	dialog.connect("not_clicked",self,"dialog_not_clicked",[],CONNECT_ONESHOT)
	dialog.display(10,current_string)
	dialog.show()


var current_string_thinking :=""
func think_delayed():
	state = state_t.thinks
	#stop()
	cloud.connect("clicked",self,"cloud_clicked",[],CONNECT_ONESHOT)
	cloud.connect("not_clicked",self,"cloud_not_clicked",[],CONNECT_ONESHOT)
	cloud.display(10,current_string_thinking)
	cloud.show()

func stop():
	character_speed = 0


func gain_speed():
	state = state_t.walks
	character_speed = 400 
	
	
func receive_depression_event(depression_event):
	var depression_action = depression_event.get_action()
	if depression_action.get_type() == enums.action_t.cptn_display:
		state = state_t.walks_while_waiting_to_display
		current_string = depression_action.get_text()
		delay_say_timer.start(1)
		if !(delay_say_timer.connect("timeout",self,"say_delayed",[],CONNECT_ONESHOT)== OK):
			print("Could not connect timeout to say_delayed")
	elif depression_action.get_type() == enums.action_t.cptn_think:
		state = state_t.walks_while_waiting_to_think
		
		current_string_thinking= depression_action.get_text()
		print("THINK:",current_string_thinking)
		delay_say_timer.start(1)
		if !(delay_say_timer.connect("timeout",self,"think_delayed",[],CONNECT_ONESHOT)== OK):
			print("Could not connect timeout to think_delayed")

#func _unhandled_input(event):
#	if not event.is_action_pressed("LeftClick"):
#		return
#	_update_navigation_path(character.position, get_local_mouse_position())


func dialog_clicked():
	Globals.click_hits +=1
	emit_signal("dialog_clicked")
	dialog.disconnect("not_clicked",self,"dialog_not_clicked")
	dialog.hide()


func dialog_not_clicked():
	Globals.click_misses +=1
	emit_signal("dialog_missed")
	dialog.disconnect("clicked",self,"dialog_clicked")
	dialog.hide()
		

func cloud_clicked():
	Globals.click_hits +=1
	cloud.disconnect("not_clicked",self,"cloud_not_clicked")
	cloud.hide()


func cloud_not_clicked():
	Globals.click_misses +=1
	cloud.disconnect("clicked",self,"cloud_clicked")
	cloud.hide()

func waste_time():
	move([700,512])
	gain_speed()
	

func _ready():
	add_child(delay_say_timer)
	dialog.hide()
	cloud.hide()
	delay_say_timer.add_to_group("RealTime")
	

func move_along_path(distance):
	var last_point = character.position
	while path.size():
		var distance_between_points = last_point.distance_to(path[0])
		# The position to move to falls between two points.
		if (distance_between_points == 0):
			print("Error division by zero!!!!! on  navigator last path was same as prelast")
			path.remove(0)
			return
		if distance <= distance_between_points:
			character.position = last_point.linear_interpolate(path[0], distance / distance_between_points)
			return
		# The position is past the end of the segment.
		distance -= distance_between_points
		last_point = path[0]
		path.remove(0)
	# The character reached the end of the path.
	character.position = last_point
	set_process(false)
	emit_signal("finished_a_move")


func _update_navigation_path(start_position, end_position):
	path = get_simple_path(start_position, end_position, true )
	#var distance = 0
	#for i in path:
	#	distance+=i.length()
	#print(distance)


	# The first point is always the start_position.
	# We don't need it in this example as it corresponds to the character's position.
	path.remove(0)
	set_process(true)
