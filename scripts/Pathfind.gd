extends PathFollow2D

const Direction = preload("common/Direction.gd")

var velocity := 60
enum state {MOVING,STOPED,HIDDEN}
var current_state:int = state.HIDDEN
var poi
var finishline = 1
signal finished

func hide_wrapper():
	hide()
	current_state = state.HIDDEN
func show_wrapper():
	show()
	current_state = state.MOVING
func start(direction):
	if direction == Direction.go.left:
		set_unit_offset(1)
		velocity   = -60
		finishline = 0
	elif direction == Direction.go.right:
		set_unit_offset(0)
		velocity = 60
		finishline = 1
	show_wrapper()
	
func maybe_move(delta):
	if(current_state == state.MOVING):
		set_offset(get_offset() + velocity * delta)
		
func check_finish():
	if(get_unit_offset() == finishline and !loop ):
		emit_signal("finished")
		hide_wrapper()

func _ready():
	hide_wrapper()
	poi  = get_parent().get_parent().poi
func _process(delta):
	if(current_state == state.HIDDEN):
		return
	
	maybe_move(delta)
	check_finish()
	
