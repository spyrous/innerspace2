extends Node
#Lets say we will have 10 events per day
# 5 at the first 3 minutes
# and 5 at the last three minutes

#Based on depression level we have to decide 
#how many events we would like to show in the day
#keeping track of :the time of the last event,the new interval
# and the ammount of events we have to show we can understand
#if we want to express a new event or now
#deciding which event is based on which are already shown 
#and which specifics of MADRS we have higher
const parametrics = preload("common/parametrics.gd")

signal depression_event
var amount_of_events_today:= 1# + int(Globals.day %7 == 0)
var thresholds_for_events_to_occure = []
var total_time = parametrics.max_minute * 60 + parametrics.max_second
var current_time = 0
var depression_expression := 0
#based on the diffrent parameters of depression
#Every week we should express 8 different parts of
#depression so every day we can count from the pool 
#of already expressed events and with the value of its 
#expression
var types_not_expressed_yet_double_index := [[1,0],[2,1],[5,2],[7,3],[8,4],[9,5]]
func select_depression_expression():
	var rng = RandomNumberGenerator.new() 
	rng.randomize()
	var event_index = rng.randi_range(0,types_not_expressed_yet_double_index.size()-1)
	var tuple_index = types_not_expressed_yet_double_index.pop_at(event_index)
	depression_expression =  tuple_index[1] * 3 + int(Globals.per_week_estimation[-1][tuple_index[0]]/2)
	print("The SELECTED DEPRESSION EXPRESSION is: ",depression_expression)


func time_for_event():
	#that constant is given for response times
	if (!thresholds_for_events_to_occure.empty() and (total_time- current_time) > thresholds_for_events_to_occure[0]):
		thresholds_for_events_to_occure.pop_front()
		return true
	else: 
		return false

# This function is triggered from a calculator of how much time 
#we have for the next minigame event
func decide_to_express_depression():
	current_time = Tick.get_seconds()
	var is_it_time = time_for_event()	
	if(is_it_time):
		select_depression_expression()
		emit_signal("depression_event",depression_expression) 

func new_day():
	var threshold = (total_time-15*1.0) / 1#amount_of_events_today
	var sum = threshold
	#TODO remove line under, just for test
	#thresholds_for_events_to_occure.push_back(4)
	for i in amount_of_events_today:
		print("Seconds for event to occure",sum)
		thresholds_for_events_to_occure.push_back(sum)
		sum=sum+threshold

