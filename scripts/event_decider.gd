extends Node

const enums = preload("common/enums.gd")
const parametrics = preload("common/parametrics.gd")

const coords = [
	 [0,256],[700,256],[1380,256]
	,[0,512],[700,512],[1380,512]
	,[0,766],[700,766]
	]
var map_positions := {
	enums.positions.keys()[enums.positions.bed]:coords[0],
	enums.positions.keys()[enums.positions.poi1]:coords[1],
	enums.positions.keys()[enums.positions.three_dee_printer]:coords[2],
	enums.positions.keys()[enums.positions.poi2]:coords[3],
	enums.positions.keys()[enums.positions.tv]:coords[4],
	enums.positions.keys()[enums.positions.table]:coords[4], #two positions might have the same coords
	enums.positions.keys()[enums.positions.poi3]:coords[5],
	enums.positions.keys()[enums.positions.accurator]:coords[6],
	enums.positions.keys()[enums.positions.trademill]:coords[7],
	enums.positions.keys()[enums.positions.mirror]:coords[0],
	}

func map_poi_to_coords(enum_poi):
	return map_positions[enums.positions.keys()[enum_poi]]

func possible_rooms_to_actions(i):
	if i == 1 :
		return enums.positions.poi1
	elif i == 2 :
		return enums.positions.three_dee_printer
	elif i == 3 :
		return enums.positions.poi2
	#elif i == 4 : it is never possible yet
	elif i == 5 :
		return enums.positions.poi3
	elif i == 6 :
		return enums.positions.accurator
#never
#	elif i == 7 :
#		return enums.positions.trademill

var show_the_mirror := false
var ate_food := false
var worked_out:= false

func new_day():
	ate_food = false
	worked_out = false
	show_the_mirror = false

func weighted_randomize_poi(possible_rooms):
#	if Globals.current_poi == enums.positions.poi3:
#		Globals.current_poi = enums.positions.poi2
#		return Globals.current_poi
#	else :
#		Globals.current_poi = enums.positions.poi3
#		return Globals.current_poi

	var prev_poi = Globals.current_poi
	if !show_the_mirror:
		print("Weighted_randomize_poi MIRROR")
		show_the_mirror = true
		Globals.current_poi =enums.positions.mirror
		return  Globals.current_poi

	if (!ate_food
	&& prev_poi != enums.positions.table
	&& Tick.get_seconds() < 0.5 * (parametrics.max_minute *60 +parametrics.max_second)):
		print("Weighted_randomize_poi FOOD")
		ate_food = true
		Globals.current_poi =  enums.positions.table
		return  Globals.current_poi

	if (!worked_out
	&& prev_poi != enums.positions.trademill
	&& Tick.get_seconds() < 0.2 * (parametrics.max_minute *60 +parametrics.max_second)):
		print("Weighted_randomize_poi WORK_OUT")
		worked_out = true
		Globals.current_poi = enums.positions.trademill
		return Globals.current_poi

	print("Weighted randomize_poi random")
	#var possible_rooms := [enums.positions.poi1,enums.positions.poi2,enums.positions.poi3,enums.positions.accurator,enums.positions.tv] #2 is three_dee_printer #4 is chill or eat # 7 is trademill
	var weights := [
					100 - Globals.metal,
					100 - Globals.bar2,
					100 - Globals.energy,
					100 - Globals.bar3,
					Globals.per_week_estimation[-1][6] #lastitude
					]
	var sumWeights := [0,0,0,0,0]
	if enums.positions.three_dee_printer in possible_rooms:
		weights.append(100 - (50*int(Globals.accurator_bought) + 50 * int(Globals.gym_bought)) )
		sumWeights.append(0)
		# possible_rooms has already been populated

	#Avoid same room
	match prev_poi:

		enums.positions.poi1:
			weights[0]=0
		enums.positions.poi2:
			weights[1]=0
		enums.positions.poi3:
			weights[2]=0
		enums.positions.accurator:
			weights[3]=0
		enums.positions.tv:
			weights[4]=0
		enums.positions.three_dee_printer:
			if  weights.size() == 6:
				weights[5]=0

	var sum := 0
	var index :=0
	for data in weights:
		sum = sum + data
		sumWeights[index] = sum
		index = index + 1 
	var random = randi() % sumWeights[-1]
	index = 0
	for data in sumWeights:
		if data >= random:
			break
		index = index + 1
	Globals.current_poi= possible_rooms[index]
	return Globals.current_poi
	
#func emit_next_event(remaining_time,current_position,possible_rooms):
#	if Globals.day % 7:
#		pass
#	pass
