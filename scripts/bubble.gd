extends Node2D

var variable_text := ""

func display(time,msg):
	label.text = ""
	show()
	variable_text = msg
	current_text = ""
	current_letter =0
	var dt = 0.75/msg.length()  #standard animation time 0.75s
	#after the message is fully displayed for the user to click
	_on_Timer_timeout()
	event_timer.start(time)
	timer.start(dt)

var do_close := false
var current_text := ""
var current_letter :=0
onready var label = get_node("VBoxContainer/Label")
onready var timer = get_node("Timer")
onready var event_timer = get_node("EventTimer")
onready var patch = get_node("VBoxContainer/Label/NinePatchRect")
signal clicked
signal not_clicked

func _ready():
	hide()
	$EventTimer.add_to_group("RealTime")
	$Timer.add_to_group("RealTime")

func _input(event):
	if (event.is_action_pressed("LeftClick") 
		&& patch.get_global_rect().has_point(event.position)):
		emit_signal("clicked")
		event_timer.stop()

func finished_presenting_text():
	if current_letter == variable_text.length() :
		return true
	else :
		return false


func _on_Timer_timeout():
	if(!do_close):
		current_text += variable_text[current_letter]
		current_letter += 1
		label.text = current_text
	do_close = finished_presenting_text()

func _on_EventTimer_timeout():
	emit_signal("not_clicked") # Replace with function body.
	event_timer.stop()
