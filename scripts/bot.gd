extends Node2D

const enums = preload("common/enums.gd")
enum state_t {displays,stands}
var state = state_t.stands

onready var dialog = $BotSprite/Dialog
signal dialog_clicked
signal dialog_missed

var current_string:=""
func receive_conversation_event(conversation_event):
	var conversation_action = conversation_event.get_action()
	if conversation_action.get_type() == enums.action_t.bot_display:
		state = state_t.displays
		current_string = conversation_action.get_text()
		dialog.connect("clicked",self,"dialog_clicked",[],CONNECT_ONESHOT)
		dialog.connect("not_clicked",self,"dialog_not_clicked",[],CONNECT_ONESHOT)
		dialog.display(10,current_string)
		dialog.show()


func dialog_clicked():
	Globals.click_hits +=1
	state = state_t.stands
	emit_signal("dialog_clicked")
	dialog.disconnect("not_clicked",self,"dialog_not_clicked")
	dialog.hide()


func dialog_not_clicked():
	Globals.click_misses +=1
	state = state_t.stands
	emit_signal("dialog_missed")
	dialog.disconnect("clicked",self,"dialog_clicked")
	dialog.hide()


func sleep():
	var connections = get_signal_connection_list("dialog_clicked")
	for conn in connections:
		disconnect(conn.signal, conn.target, conn.method)
	connections = get_signal_connection_list("dialog_missed")
	for conn in connections:
		disconnect(conn.signal, conn.target, conn.method)
	match state:
		state_t.displays:
			dialog.hide()
		state_t.stands:
			pass
	state = state_t.stands


func _ready():
	dialog.hide()
