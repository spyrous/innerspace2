extends Node2D

signal accurator_bought
signal gym_bought
signal general_finished
signal stop_time

var upgrades := {"Accurator":15,"Gym":20}
var upgraded := [Globals.accurator_bought,Globals.gym_bought]
onready var accurator_button = $Button0
onready var gym_button = $Button1
onready var metalLabel = $Metal

func init():
	metalLabel.text = "Metal: "+ String(Globals.metal)

func start():
	emit_signal("stop_time")
	if upgraded[0] :
		accurator_button.set_disabled(true)
		accurator_button.text = "Accurator upgrade bought"
	if upgraded[1]: 
		gym_button.set_disabled(true)
		gym_button.text = "Trademill upgrade bought"

func _on_Button0_pressed():
	if(Globals.metal >=15 && !upgraded[0]):
		Globals.metal = Globals.metal - 15
		upgraded[0]=true
		emit_signal("accurator_bought")
		accurator_button.set_disabled(true)
		accurator_button.text = "Accurator upgrade bought"
		metalLabel.text = "Metal: "+ String(Globals.metal)
		Globals.accurator_bought = true

func _on_Button1_pressed():
	if(Globals.metal >=15 && !upgraded[1]):
		Globals.metal = Globals.metal - 20
		upgraded[1]=true
		emit_signal("gym_bought")
		gym_button.set_disabled(true)
		gym_button.text = "Trademill upgrade bought"
		metalLabel.text = "Metal: "+ String(Globals.metal)
		Globals.gym_bought = true

func _on_Exit_pressed():
	emit_signal("general_finished",0)


func force_quit():
	emit_signal("general_finished",0)
