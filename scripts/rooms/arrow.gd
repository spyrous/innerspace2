extends Sprite
var direction :=1
const halfArc = 2 * PI * 0.175

func getArc():
	return rotation 

func _process(delta):
	if(rotation > halfArc or rotation < -halfArc):
		direction = -1*direction
	rotation += direction * delta*1.1

