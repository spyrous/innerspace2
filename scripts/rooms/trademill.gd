extends Node2D

onready var border = $border
onready var percentage = $percentage
onready var info = $info
onready var info_label = $info_label
var time:=0.0

signal general_finished

func force_quit():
	emit_signal("general_finished",min(5,5 * (percentage.get_size().y/max_height)))

const max_height = 473.0
const max_time = 4.0 #fist second is for the message
var counter := 0
var lastitude_modifier := 0.14


func _process(delta):
	if !percentage.is_visible() || percentage.get_size().y == 0:
		return
	var percentage_dt = (delta) / max_time  
	percentage.set_size(Vector2(percentage.get_size().x, max( percentage.get_size().y - percentage_dt * (max_height/2), 0))) 
	time +=delta
	if time >3.9:
		print("finished trademill",5 * (percentage.get_size().y/max_height))
		emit_signal("general_finished",5 * (percentage.get_size().y/max_height))


func _input(event):
	if (event.is_action_pressed("LeftClick") 
		&& border.get_global_rect().has_point(event.position)):
		#emit_signal("clicked")
		counter+=1
		var increased = percentage.get_size().y + lastitude_modifier * (max_height/2)
		if increased > max_height:
			increased = max_height
		percentage.set_size(Vector2(percentage.get_size().x, increased )) 
		if(percentage.get_size().y/max_height >=0.95):
			emit_signal("general_finished",5)


func display_message_and_calculate_lasstitude():
	if Globals.per_week_estimation[-1][6] == 0:
		lastitude_modifier = 0.23 + int(Globals.gym_bought)*0.09
		return "I can't wait to run!"
	elif Globals.per_week_estimation[-1][6] == 1:
		lastitude_modifier = 0.20 + int(Globals.gym_bought)*0.09
		return "Let's do it"
	elif Globals.per_week_estimation[-1][6] == 2:
		lastitude_modifier = 0.17 + int(Globals.gym_bought)*0.09
		return "Oh I don't feel like it but if it's mandatory."
	elif Globals.per_week_estimation[-1][6] == 3:
		lastitude_modifier = 0.14 + int(Globals.gym_bought)*0.09
		return "Oof, every day the same thing"
	elif Globals.per_week_estimation[-1][6] == 4:
		lastitude_modifier = 0.11 + int(Globals.gym_bought)*0.09
		return "I am so bored!"
	elif Globals.per_week_estimation[-1][6] == 5:
		lastitude_modifier = 0.08 + int(Globals.gym_bought)*0.09
		return "I really don't feel like doing that"
	elif Globals.per_week_estimation[-1][6] == 6:
		lastitude_modifier = 0.05 + int(Globals.gym_bought)*0.09
		return "I will not do that"


func init():
	if Globals.gym_bought:
		percentage.set_frame_color(Color(27/255.0,144.0/255,247.0/255,1))
	else :
		percentage.set_frame_color(Color(137/255.0,247.0/255,27.0/255,1))
	border.hide()
	info.hide()
	info.hide()
	percentage.hide()
	percentage.set_size(Vector2(percentage.get_size().x,  0.15 * max_height)) 
	$Bubble.display(3,display_message_and_calculate_lasstitude())
	yield(get_tree().create_timer(1.0), "timeout")
	time = 0
	border.show()
	percentage.show()
	info.show()
	info_label.show()
	yield(get_tree().create_timer(1.0), "timeout")
	$Bubble.hide()
	
