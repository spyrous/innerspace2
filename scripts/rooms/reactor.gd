extends Node2D

var propane_color = "EF671B"
var argon_color = "3187DE"
var oxygen_color = "5B5249"
var hydrogen_color = "D8A333"
var nitrogen_color = "2B8A76"
var points := 0
signal general_finished
onready var up = $Reactor/up
onready var down = $Reactor/down

var color_array = [propane_color,argon_color,oxygen_color,hydrogen_color,nitrogen_color]
var indexes = [0,0]

func force_quit():
	emit_signal("general_finished",points*2)

#Silly silly code that keeps two of 5 elements
func pick_two_colors():
	var arr = [0,1,2,3,4]
	var rng = RandomNumberGenerator.new() 
	rng.randomize()
	arr.remove(rng.randi()%4)
	arr.remove(rng.randi()%3)
	arr.remove(rng.randi()%2)
	return arr

func zero_reached():
	if 0 in indexes:
		points+=1
	if points==2:
		emit_signal("general_finished",5)

func one_reached():
	if 1 in indexes:
		points+=1
	if points==2:
		emit_signal("general_finished",5)

func two_reached():
	if 2 in indexes:
		points+=1
	if points==2:
		emit_signal("general_finished",5)

func three_reached():
	if 3 in indexes:
		points+=1
	if points==2:
		emit_signal("general_finished",5)

func four_reached():
	if 4 in indexes:
		points+=1
	if points==2:
		emit_signal("general_finished",5)

	
func _ready():

	if ($Propane.connect("reached",self,"zero_reached") != OK or
	$Argon.connect("reached",self,"one_reached") !=OK or
	$Oxygen.connect("reached",self,"two_reached") !=OK or
	$Hydrogen.connect("reached",self,"three_reached")  !=OK or
	$Nitrogen.connect("reached",self,"four_reached") !=OK):
		print("Bottles couldn't initialize correct")
	indexes= pick_two_colors()
	up.color = Color(color_array[indexes[0]])
	down.color= Color(color_array[indexes[1]])
	points=0
	$Propane.init()
	$Argon.init()
	$Oxygen.init()
	$Hydrogen.init()
	$Nitrogen.init()
