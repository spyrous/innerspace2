extends Node2D

signal general_finished

onready var pizzas = ["res://assets/mocks/5pieces.png",
					"res://assets/mocks/4pieces.png",
					"res://assets/mocks/3pieces.png",
					"res://assets/mocks/2pieces.png",
					"res://assets/mocks/1piece.png",
					"res://assets/mocks/0pieces.png",
					]
onready var full_pizza = "res://assets/mocks/6pieces.png"					

func force_quit():
	pass


func show_food():
		$Cover.hide()
		$Plate.show()
		$CoverSide.hide()

func display_message():
	if Globals.per_week_estimation[-1][4] == 0:
		return "MMMM I like it!"
	elif Globals.per_week_estimation[-1][4] == 1:
		return "Nice"
	elif Globals.per_week_estimation[-1][4] == 2:
		return "Oh Food."
	elif Globals.per_week_estimation[-1][4] == 3:
		return "I have to eat I suppose."
	elif Globals.per_week_estimation[-1][4] == 4:
		return "Tasteless :("
	elif Globals.per_week_estimation[-1][4] == 5:
		return "I don't want any"
	elif Globals.per_week_estimation[-1][4] == 6:
		return "I refuse to eat that!"

func init():
	$Piza.set_texture(load(full_pizza))
	$Bubble.display(1," ")
	yield(get_tree().create_timer(1.0), "timeout")
	show_food()
	$Bubble.display(3,display_message())
	for n in  (6 - Globals.per_week_estimation[-1][4]):
		yield(get_tree().create_timer(1.0), "timeout")
		$Piza.set_texture(load(pizzas[n]))
	yield(get_tree().create_timer(1.0), "timeout")
	emit_signal("general_finished",0)
