extends Node2D

onready var arrow = $Arrow
var upgraded = preload("res://assets/upgraded_accurator.png")
var halfCorrect = 	2*PI*0.051
signal general_finished
func init():

	randomize()

	if Globals.accurator_bought :
		halfCorrect = 2*PI* 0.071
		$Gauge.set_texture(upgraded)
		$Gauge.set_global_position(Vector2(918.999,746))

func force_quit():
	emit_signal("general_finished",0)



func _unhandled_input(event):
	if not event.is_action_pressed("LeftClick"):
		return
	if(arrow.getArc() < halfCorrect and arrow.getArc() > -halfCorrect):
		print("Correct")
		emit_signal("general_finished",5)
	else :
		emit_signal("general_finished",0)
		print("Wrong")
