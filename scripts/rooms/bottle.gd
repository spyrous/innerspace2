extends RigidBody2D

signal clicked

var held = false

func _input_event(_viewport, event,_shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			print("clicked")
			held=true
			emit_signal("clicked", self)




func pickup():
	if held:
		return
	#mode = RigidBody2D.MODE_STATIC
	held = true

func drop(impulse=Vector2.ZERO):
	if held:
		mode = RigidBody2D.MODE_RIGID
		apply_central_impulse(impulse)
		held = false
