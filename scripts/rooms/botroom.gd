extends Node2D

onready var prev_degrees := 0 
onready var cog = $border/Cog
onready var warning = $warning
onready var initial_degrees
signal  general_finished
	
var elapsed := 0.0
var warning_trigger := false

func _physics_process(_delta):
	if ! $Control.get_global_rect().has_point((get_global_mouse_position())):
		cog.look_at(get_global_mouse_position())
		if prev_degrees > cog.rotation_degrees : 
			warning_trigger = true
			warning.show()
		if warning_trigger:
			elapsed += _delta
		if elapsed  > 0.9:
			warning_trigger=false
			warning.hide()
			elapsed=0
		prev_degrees = cog.rotation_degrees
		if cog.rotation_degrees >5000:
			emit_signal("general_finished",5)


func force_quit():
	finish()

func finish():
	print("SCORE",cog.rotation_degrees)
	emit_signal("general_finished",clamp(int(cog.rotation_degrees/1000),0,5))
	 

func init():
	cog.rotation_degrees = 0
	prev_degrees = cog.rotation_degrees
	initial_degrees = cog.rotation_degrees
	yield(get_tree().create_timer(5.0), "timeout")
	finish()
