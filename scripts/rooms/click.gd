extends Node2D
export(PackedScene) var asteroid_scene 

signal general_finished

onready var border = $Path2D/PathFollow2D
onready var control = $Control
onready var canvas_poly = $Polygon2D

onready var ast_array = [asteroid_scene.instance()
				,asteroid_scene.instance()
				,asteroid_scene.instance()
				,asteroid_scene.instance()]

var total_points = 0

func add_point():
	total_points+=1
	if total_points == 4:
		emit_signal("general_finished",4 )

func _input(event):
	if (event.is_action_pressed("LeftClick") 
		&& control.get_global_rect().has_point(event.position)):
		for i in ast_array:
			if i.test_if_clicked(event.position):
				i.hide()
				add_point()

		
func renew_asteroid(asteroid):
	border.offset = randi()
	var direction = border.rotation + PI / 2
	asteroid.position = (border.position)
	direction += rand_range(-PI / 9, PI / 9)
	asteroid.rotation = direction
	var velocity = Vector2(rand_range(250.0, 650.0), 0.0)
	asteroid.linear_velocity = velocity.rotated(direction)
	asteroid.show()

func init():
	randomize()
	total_points = 0
	for i in ast_array:
		renew_asteroid(i)


func _ready():
	for i in ast_array:
		canvas_poly.add_child(i)

func force_quit():
	emit_signal("general_finished",total_points)
