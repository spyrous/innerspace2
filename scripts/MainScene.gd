extends Node2D

const Direction = preload("common/Direction.gd")

var current_step : int = 0
var rute = []
onready var routes = [[$Deck00/Path/Pathfinder,$Deck01/Path/Pathfinder,$Deck02/Path/Pathfinder],
					  [$Deck10/Path/Pathfinder,$Deck11/Path/Pathfinder,$Deck12/Path/Pathfinder],
					  [$Deck20/Path/Pathfinder,$Deck21/Path/Pathfinder,null]]

func decide_route():
	return [6,7]

func step():
	if current_step < rute.size()-1:
		var x0 = rute[current_step][0]
		var y0 = rute[current_step][1]
		var x1 = rute[current_step+1][0]
		var y1 = rute[current_step+1][1]
		var dir = Direction.go.down
		if x0 > x1:
			dir = Direction.go.left
		elif x0 < x1:
			dir = Direction.go.right
		elif y0 < y1:
			dir = Direction.go.up
		print("Step:",current_step,"Connecting:[",x0,",",y0,"] finish with:[",x1,",",y1,"] start dir:",dir)
		routes[x0][y0].connect("finished",routes[x1][y1],"start",[dir],CONNECT_ONESHOT)
		routes[x0][y0].connect("finished",self,"step",[],CONNECT_ONESHOT)
		current_step = current_step + 1 
	
func _ready():
	rute = decide_route()
	step()
	routes[rute[0][0]][rute[0][1]].start(Direction.go.right)
	
	
