extends Control

onready var label = $info_label
onready var nameL = $name

func get_score():
	return $ProgressBar.value

var local_id=null
func update():
	match local_id:
		1:
			$ProgressBar.value = Globals.metal
			label.text = str(Globals.metal)
			nameL.text = "Metal"
		2: 
			$ProgressBar.value = Globals.bar2
			label.text = str(Globals.bar2)
			nameL.text = "Bot Baterry"
		3: 
			$ProgressBar.value = Globals.bar3
			label.text = str(Globals.bar3)
			nameL.text = "Energy"
		4: 
			$ProgressBar.value = Globals.bar4
			label.text = str(Globals.bar4)
			nameL.text = "Route"
		5: 
			$ProgressBar.value = Globals.bar5
			label.text = str(Globals.bar5)
			nameL.text = "Fitness"
		
func update_globals():
	match local_id:
		1:
			Globals.metal = $ProgressBar.value  
		2: 
			Globals.bar2 = $ProgressBar.value  
		3: 
			Globals.bar3 = $ProgressBar.value  
		4: 
			Globals.bar4 = $ProgressBar.value  
		5: 
			Globals.bar5 = $ProgressBar.value  


func score(score):
	#prstr("Progress Bar:",$ProgressBar.value," | ",$ProgressBar.min_value," | ",$ProgressBar.max_value,score)
	#range zero to hundred
	$ProgressBar.value = min($ProgressBar.value+score,100)
	label.text = str($ProgressBar.value)
	update_globals()

func init(id):
	local_id = id
	update()	
		
