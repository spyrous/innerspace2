extends Node2D

onready var label = $presenter/label

func update():
	print("Updating day")
	label.text = "DAY:"+String(Globals.day).pad_zeros(2)

func _ready():
	update()

