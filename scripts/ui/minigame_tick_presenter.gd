extends Control

signal finished

var total := 0.0
var passed:= 0.0 

func start(total_seconds:int):
	total = total_seconds
	passed= 0

func _process(delta):
	passed+=delta
	$label.text= str(int(total-passed)).pad_zeros(2)
	if passed > total :
		emit_signal("finished",0)


