extends Node2D

signal night_finished
signal light_down
signal light_up

onready var presenter =$presenter

func express_sleep_reduction():
	var start = 0 
	var end = 7
	apply_scale(Vector2(2,2))
	if Globals.per_week_estimation.back()[3] == 0 :
		pass
	elif Globals.per_week_estimation.back()[3] == 1:
		start = 0.25
	elif Globals.per_week_estimation.back()[3] == 2:
		start = 0.25
		end = 6.75
	elif Globals.per_week_estimation.back()[3] == 3:
		start = 0.5
		end = 6.5
	elif Globals.per_week_estimation.back()[3] == 4:
		start = 1
		end = 6
	elif Globals.per_week_estimation.back()[3] == 5:
		start = 1.5
		end = 5.5
	elif Globals.per_week_estimation.back()[3] == 6:
		start = 2
		end = 5
	presenter.display_sleep_reduction(start,end)
	
func emit_light_up():
	emit_signal("light_up")

func emit_light_down():
	emit_signal("light_down")

func emit_night_finished():
	apply_scale(Vector2(0.5,0.5))
	emit_signal("night_finished")

func _ready():
	presenter.connect("light_up",self,"emit_light_up")
	presenter.connect("light_down",self,"emit_light_down")
	presenter.connect("night_finished",self,"emit_night_finished")
