extends Node2D

const questions := [
	""" Daily Psychometric Report:
Describe subjects psychological condition by answering every
question, at night your report will be submit automatically.""",
	"""1) Aparent Sadness
Representing despondency, gloom and despair, (more than just
ordinary transient low spirits) reflected in speech, facial 
expression, and posture.
Rate by depth and inability to brighten up.:""",
	"""2) Reported Sadness
Representing reports of depressed mood, regardless of whether
it is reflected in appearance or not. Includes low spirits, 
despondency or the feeling of being beyond help and without 
hope. Rate according to intensity, duration and the extent to
which the mood is reported to be influenced by events. """,
	"""3) Inner Tension 
Representing feelings of ill-defined discomfort, edginess, 
inner turmoil, mental tension mounting to either panic, dread
or anguish. Rate according to intensity, frequency, duration
and the extent of reassurance called for.""",
	"""4) Reduced Sleep 
Representing the experience of reduced duration or depth of 
sleep compared to the subject&rsquo;s own normal pattern when
well.""",
	"""5) Reduced Appetite 
Representing the feeling of a loss of appetite compared with
when well. Rate by loss of desire for food or the need to 
force oneself to eat.""",
	"""6) Concentration Difficulties 
Representing difficulties in collecting one&rsquo;s thoughts
mounting to incapacitating lack of concentration. Rate 
according to intensity, frequency, and degree of incapacity 
produced.""",
	"""7) Lassitude
Representing a difficulty getting started or slowness 
initiating and performing everyday activities.""",
	"""8) Inability to Feel 
Representing the subjective experience of reduced interest in
the surroundings, or activities that normally give pleasure.
The ability to react with adequate emotion to circumstances 
or people is reduced.""",
	"""9) Pessimistic Thoughts 
Representing thoughts of guilt, inferiority, self-reproach, 
sinfulness, remorse and ruin.""",
	"""10) Suicidal Thoughts 
Representing the feeling that life is not worth living, that
a natural death would be welcome, suicidal thoughts, and 
preparations for suicide. Suicidal attempts should not in 
themselves influence the rating.""",
	]
const answers0 := [
	"",
	"0) No sadness.                                                                 ",
	"0) Occasional sadness in keeping with the circumstances.                       ",
	"0) Placid. Only fleeting inner tension.                                        ",
	"0) Sleeps as usual.                                                            ",
	"0) Normal or increased appetite.                                               ",
	"0) No difficulties in concentrating.                                           ",
	"0) Hardly any difficulties in getting started. No sluggishness.                ",
	"0) Normal interest in the surroundings and in other people.                    ",
	"0) No pessimistic thoughts.                                                    ",
	"0) Enjoys life or takes it as it comes.                                        ",]
const answers2 = [
	"",
	"2) Looks dispirited but does brighten up without difficulty.                   ",
	"2) Sad or low but brightens up without difficulty.                             ",
	"2) Occasional feelings of edginess and ill-defined discomfort.                 ",
	"2) Slight difficulty dropping off to sleep or slightly reduced, light or fitful sleep.",
	"2) Slightly reduced appetite.                                                  ",
	"2) Occasional difficulties in collecting one’s thoughts.                       ",
	"2) Difficulties in starting activities.                                        ",
	"2) Reduced ability to enjoy usual interests.                                   ",
	"2) Fluctuating ideas of failure, self-reproach or self-depreciation.           ",
	"2) Weary of life. Only fleeting suicidal thoughts.                             ",]
const answers4 = [
	"",
	"4) Loss of interest in the surroundings.Loss of feelings for friends and       acquaintances.",
	"4) Pervasive feelings of sadness or gloominess. The mood is still influenced by external circumstances.",
	"4) Appears sad and unhappy most of the time.                                   ",
	"4) Continuous feelings of inner tension or intermittent panic which the patient can only master with some difficulty.",
	"4) Sleep reduced or broken by at least two hours.                               ",
	"4) No appetite. Food is tasteless.                                              ",
	"4) Difficulties in concentrating and sustaining thought which reduces ability   to read or hold a conversation.",
	"4) Difficulties in starting simple routine activities, which are carried out    with effort.",
	"4) Persistent self-accusations, or definite but still rational ideas of guilt   or sin. Increasingly pessimistic about the future.",
	"4) Probably better off dead. Suicidal thoughts are common, and suicide is       considered as a possible solution, but without specific plans or intention.",]
const answers6 = [
	"",
	"6) Continuous or unvarying sadness, misery or despondency.                      ",
	"6) Looks miserable all the time. Extremely despondent.                          ",
	"6) Unrelenting dread or anguish. Overwhelming panic.                            ",
	"6) Less than two or three hours sleep.                                          ",
	"6) Needs persuasion to eat at all.                                              ",
	"6) Unable to read or converse without great difficulty.                         ",
	"6) Complete lassitude. Unable to do anything without help.                      ",
	"6) The experience of being emotionally paralyzed, inability to feel anger,grief or pleasure and a complete or even painful failure to feel for close friends. ",
	"6) Delusions of ruin, remorse and irredeemable sin. Self-accusations which are  absurd and unshakable.",
	"6) Explicit plans for suicide when there is an opportunity. Active preparations for suicide.",
	]
onready var arrows := [
					   $arrow0,
					   $arrow1,
					   $arrow2,
					   $arrow3,
					   $arrow4,
					   $arrow5,
					   $arrow6,
					  ]

onready var choices := $Choices
onready var question := $Question
onready var prev := $prev
onready var next := $next
onready var pageLabel := $pageLabel

func _ready():
	choices.grab_focus()
	update_view()

func update_view():
	pageLabel.text  = String(Globals.current_page)
	if Globals.current_page == 0:
		prev.hide()
	else :
		prev.show()
	if Globals.current_page == 10 :
		next.hide()
	else :
		next.show()
	choices.select(Globals.selection_per_page[Globals.current_page],true)
	update_selection(Globals.selection_per_page[Globals.current_page],Globals.current_page != 0)
	question.text = questions[Globals.current_page]
	if(Globals.current_page != 0):
		choices.set_item_text(0,answers0[Globals.current_page])
		choices.set_item_text(1,"1)                                                                             ")
		choices.set_item_text(2,answers2[Globals.current_page])
		choices.set_item_text(3,"3)                                                                             ")
		choices.set_item_text(4,answers4[Globals.current_page])
		choices.set_item_text(5,"5)                                                                             ")
		choices.set_item_text(6,answers6[Globals.current_page])
	else :
		choices.set_item_text(0,"")
		choices.set_item_text(1,"")
		choices.set_item_text(2,"")
		choices.set_item_text(3,"")
		choices.set_item_text(4,"")
		choices.set_item_text(5,"")
		choices.set_item_text(6,"")

	#print("Selection per page:",Globals.selection_per_page)


func _unhandled_input(event):
	if event.is_action_pressed("ui_left") && Globals.current_page != 0:
		Globals.current_page = Globals.current_page - 1
		update_view()
		return
	if event.is_action_pressed("ui_right") && Globals.current_page != 10:
		Globals.current_page = Globals.current_page + 1
		update_view()
		return


func update_selection(index:int,maybe:bool):
	var i=0
	for arrow in arrows:
		if i == index  && maybe:
			arrow.show()
		else:
			arrow.hide()
		i = i + 1


func _on_Choices_item_selected(index:int):
	Globals.selection_per_page[Globals.current_page] = index
	update_selection(index,Globals.current_page!=0)
