extends Control


func _on_Play_pressed():
	ButtonSound.play()
	if !(	get_tree().change_scene("res://scenes/main.tscn") == OK):
		print("Could not change_scene to main from ui/start")

func _on_Help_pressed():
	ButtonSound.play()
	if !(	get_tree().change_scene("res://scenes/ui/help.tscn") == OK):
		print("Could not change scene to help from ui/start")
