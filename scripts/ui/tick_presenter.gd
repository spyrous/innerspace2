extends Control

signal light_up
signal light_down
signal night_finished

const parametrics = preload("../common/parametrics.gd")

var dt_mapping := 0.1
var max_tick := parametrics.max_minute * 60 + parametrics.max_second

var is_night := false
var night_phases := [false,false]
var sleep_start := 0.0
var sleep_end := 0.0
var night_tick := 0.0
#night will take 7 seconds
func display_sleep_reduction(start,end):
	is_night = true
	night_phases = [false,false]
	sleep_start = start
	sleep_end = end
	night_tick = 0.0
	emit_signal("light_up")

func pause():
	Tick.pause()

func _process(delta):
	if(is_night):
		night_tick+=delta
		if(night_tick > sleep_start && !night_phases[0]):
			night_phases[0] = true
			emit_signal("light_down")
		elif (night_tick > sleep_end && !night_phases[1]):
			night_phases[1] = true
			emit_signal("light_up")
		elif night_tick > 7 :
			is_night=false
			emit_signal("night_finished")
		$label.text = str(int(night_tick)).pad_zeros(2) + str(":") +str(int( (night_tick - int(night_tick))*60 )).pad_zeros(2)
		return

	var day_domain_minutes = (max_tick - (Tick.minutes * 60 + Tick.seconds)) * dt_mapping

	$label.text= str(int(7+(day_domain_minutes/60))).pad_zeros(2) + str(":") + str(int(fmod(day_domain_minutes ,60))).pad_zeros(2)
	if Tick.minutes == 0:
		$label.set("custom_colors/font_color", Color(1,0.2,0.2))

# tick domain means real tick class and day domain means the visible part
func tick_domain_to_day_domain():
	var max_day  := 17.0 * 60.0 #because 17 hours is the normal to be awake
	dt_mapping = max_day / max_tick
	print("Dt mapping is :",dt_mapping)

func _ready():
	tick_domain_to_day_domain()

