extends Node

const enums = preload("common/enums.gd")

class action:
	var type setget ,get_type
	func get_type():
		return type
	func _init(t):
		self.type = t

class go_action extends action:
	var coords=[] 
	func get_coords():
		return coords
	func _init(coords_).(enums.action_t.go_to):
		self.coords=coords_

class captain_display_action extends action:
	var text:="" setget ,get_text
	func _init(t_).(enums.action_t.cptn_display):
		self.text = t_
	func get_text():
		return text
		
class captain_think_action extends action:
	var text:="" setget ,get_text
	func _init(t_).(enums.action_t.cptn_think):
		self.text = t_
	func get_text():
		return text
		
class bot_display_action extends action:
	var text:="" setget ,get_text
	func _init(t_).(enums.action_t.bot_display):
		self.text = t_
	func get_text():
		return text
var go_to_00 = go_action.new([0,0])
var go_to_01 = go_action.new([0,1])
var go_to_02 = go_action.new([0,2])
var cont = action.new(enums.action_t.cont)
var stall = action.new(enums.action_t.stall)
# Depression events should have their own type and
# custom interface every dialog is a tree every element of the tree
# may have an optional exit action and two optional next states with optional next actions 
class tree:
	var actio setget , get_action
	var childs
	func child(index):
		return childs[index]
	func _init(a=null,childs_=null):
		self.actio = a
		self.childs = childs_ 
	func get_action():
		return actio
	func display():
		print("Action type is: ",enums.action_t.keys()[actio.get_type()])

var cont_tree = tree.new(action.new(enums.action_t.cont))
var stall_tree = tree.new(action.new(enums.action_t.stall))

class captain_display_tree extends tree:
	func _init(msg).(captain_display_action.new(msg),[tree.new(action.new(enums.action_t.cont)),tree.new(action.new(enums.action_t.cont))]):
		pass


class captain_think_tree extends tree:
	func _init(msg).(captain_think_action.new(msg),[tree.new(action.new(enums.action_t.cont)),tree.new(action.new(enums.action_t.cont))]):
		pass

class bot_display_tree extends tree:
	func _init(msg).(bot_display_action.new(msg),[tree.new(action.new(enums.action_t.cont)),tree.new(action.new(enums.action_t.cont))]):
		pass

class captain_bot_speach extends tree:
	func _init(cptn_msg,bot_msg).(captain_display_action.new(cptn_msg),[tree.new(action.new(enums.action_t.cont)),bot_display_tree.new(bot_msg)]):
		pass


class bot_captain_speach extends tree:
	func _init(bot_msg,cptn_msg).(bot_display_action.new(bot_msg),[tree.new(action.new(enums.action_t.cont)),captain_display_tree.new(cptn_msg)]):
		pass

enum depression_types{
	apparent_sadness,
	reported_sadness,
	inner_tension,
	sleep,
	appetite,
	concentration,
	lassitude,
	inabillity_to_feel,
	pessimistic_thoughts,
	suicidial
	}
enum severity{
	zero,one,two,three,four,five,six
	}

class depression_event:
	var _type
	var _sever
	var _tree setget , get_root
	func _init(type,sever,tree):
		self._type = type
		self._sever= sever
		self._tree = tree
	func display():
		return _tree.display()
	func get_root():
		return _tree

#var cptn_display00= captain_display_action.new("I Feel tired")
#var bot_display00 = bot_display_action.new("Continue!")
##left is ignore , right is click 
#var event0_root = tree.new(cptn_display00,
#						[stall_tree,tree.new(bot_display00,[stall_tree,cont_tree])]
#						)
#var event0 = depression_event.new(depression_types.lassitude,severity.zero,event0_root)
#
#var bot_display01= bot_display_action.new("You seem a bit sader than usual :(")
#var cptn_display01= captain_display_action.new("Yeah if you couldn't spam it all the time...I am fine.")
#var bot_display01b= bot_display_action.new("Sorry for that, I just care for you.")
#var cptn_display01b= captain_display_action.new("Fine let's continue.")
#var cptn_display01c= captain_display_action.new("Thanks, I don't care for either of us")
#var event1_root = tree.new(bot_display01,
#						[stall_tree,tree.new(cptn_display01,
#							[stall_tree,tree.new(bot_display01b,[
#								tree.new(cptn_display01b,[stall_tree,cont_tree]),
#								tree.new(cptn_display01c,[stall_tree,cont_tree])]
#								)]
#						)]
#					)
#var event1 = depression_event.new(depression_types.reported_sadness,severity.three,event1_root)
var bot_display00= bot_display_action.new("You look good!:)")
var cptn_display00= captain_display_action.new("You tou little one!")
var event0_root = tree.new(bot_display00,
					[cont_tree,captain_display_tree.new("You too little one")])

var event0 = depression_event.new(depression_types.reported_sadness,severity.zero,event0_root)

var bot_display01 = bot_display_action.new("You seem a bit sader than usuall")
var cptn_display01= captain_display_tree.new("Yeah if you couldn't spam it all the time...I am fine. ")
var event1_root = tree.new(bot_display01,
					[cont_tree,cptn_display01])
var event1 = depression_event.new(depression_types.reported_sadness,severity.three,event1_root)

var bot_display02 = bot_display_action.new("Hey what's with that face?\nYou seem mellancholic")
var cptn_display02= captain_display_tree.new("Not your problem.")
var event2_root = tree.new(bot_display02,[cont_tree,cptn_display02])
var event2 = depression_event.new(depression_types.reported_sadness,severity.six,event2_root)

var event3_root = captain_bot_speach.new("Ahh such a chill day","I like your vibes captain")
var event3 = depression_event.new(depression_types.inner_tension,severity.zero,event3_root)

var event4_root = captain_bot_speach.new("Bot can we really handle the trip?\nI don't feel like it","Relax we are worthy")
var event4 = depression_event.new(depression_types.inner_tension,severity.three,event4_root)


var event5_root = captain_bot_speach.new("BOT WE WILL DIE HERE","I can't make you\n relax,can I?")
var event5 = depression_event.new(depression_types.inner_tension,severity.six,event5_root)


var event6_root = captain_bot_speach.new("We have to do it, for our own good!","Thats the spirit")#Concentration 0
var event6 = depression_event.new(depression_types.concentration,severity.zero,event6_root)

var event7_root = captain_bot_speach.new("What are we suppposed to do?","We just have to keep going")
var event7 = depression_event.new(depression_types.concentration,severity.three,event7_root)

var cptn_display08 = captain_think_action.new("....I don't know what to do")
var event8_root = tree.new(cptn_display08,[cont_tree,cont_tree])
var event8 = depression_event.new(depression_types.concentration,severity.six,event8_root)


var event9_root = bot_captain_speach.new("Hey I just got a message,\nSammy the engineer says hi!","Quickly respond, I miss her")
var event9 = depression_event.new(depression_types.inabillity_to_feel,severity.zero,event9_root)

var event10_root = bot_captain_speach.new("Hey I just got a message,\nSammy the engineer says hi!","Whatever")
var event10 = depression_event.new(depression_types.inabillity_to_feel,severity.three,event10_root)

var event11_root = bot_captain_speach.new("Hey I just got a message, your daughter graduated!","Yeah whatever")
var event11 = depression_event.new(depression_types.inabillity_to_feel,severity.six,event11_root)

var event12_root = captain_think_tree.new("I think today will be fine")
var event12 = depression_event.new(depression_types.pessimistic_thoughts,severity.zero,event12_root)

var event13_root = captain_think_tree.new("I think today will be bad")
var event13 = depression_event.new(depression_types.pessimistic_thoughts,severity.three,event13_root)


var event14_root = captain_think_tree.new("I don't think I will ever return, and it's all my fault")
var event14 = depression_event.new(depression_types.pessimistic_thoughts,severity.six,event14_root)


var event15_root = captain_display_tree.new("Life is good.")
var event15 = depression_event.new(depression_types.suicidial,severity.zero,event15_root)


var event16_root = captain_bot_speach.new("Life is vane...what if...?", "You are scaring me!")
var event16 = depression_event.new(depression_types.suicidial,severity.three,event16_root)


var event17_root = captain_think_tree.new("Life is vane, when we reach back home \nI''ll settle it once and for all")
var event17 = depression_event.new(depression_types.suicidial,severity.six,event17_root)

var depression_array = [
	event0,
	event1,
	event2,
	event3,
	event4,
	event5,
	event6,
	event7,
	event8,
	event9,
	event10,
	event11,
	event12,
	event13,
	event14,
	event15,
	event16,
	event17,
	] 

func get(i):
	return depression_array[i]
