extends Popup
#This scene receives generalized signal: "general_finished"
#that has variantarguments , but emits diffrent signals to the main
#atm:["finished",score] and ["idled"]
signal finished
signal accurator_bought

var enums  =preload("common/enums.gd")

var current_action = enums.positions.bed
var current_scene = null
var state := false
#Rooms are on a 2D grid so we need a mapping with scenes and times to load
var scene_bed := preload("res://scenes/rooms/accuracy.tscn").instance()
var scene_poi1 := preload("res://scenes/rooms/click.tscn").instance()
var scene_three_dee_printer := preload("res://scenes/rooms/shop.tscn").instance()
var scene_poi2 := preload("res://scenes/rooms/botroom.tscn").instance()
var scene_tv := preload("res://scenes/rooms/chill.tscn").instance()
var scene_poi3_helper := preload("res://scenes/rooms/reactor.tscn")
var scene_poi3= scene_poi3_helper.instance()
var scene_accurator := preload("res://scenes/rooms/accuracy.tscn").instance()
var scene_trademill := preload("res://scenes/rooms/trademill.tscn").instance()
var scene_table := preload("res://scenes/rooms/eat.tscn").instance()
var scene_mirror := preload("res://scenes/rooms/mirror.tscn").instance()

func look_mirror():

	var rt = get_tree().get_nodes_in_group("RealTime")
	for i in rt:
		i.set_paused(true)
	current_scene = scene_mirror
	current_action = enums.positions.mirror
	add_child(scene_mirror)
	move_child(scene_mirror,0)

	$minigame_tick.show()
	$minigame_tick/presenter.start(5)
	if $minigame_tick/presenter.connect("finished",self,"timer_finished") != OK:
		push_warning("Could not connect")
	Tick.pause()
	scene_mirror.init()
	self.show()

	current_scene.connect("general_finished",self,"scene_finished")


func actions_to_scenes(i):
	if i == enums.positions.mirror  :
		return scene_mirror
	if i == enums.positions.poi1:
		return scene_poi1
	elif i == enums.positions.three_dee_printer:
		return scene_three_dee_printer
	elif i == enums.positions.poi2:
		return scene_poi2
	elif i == enums.positions.tv:
		return scene_tv
	elif i == enums.positions.table:
		return scene_table
	elif i == enums.positions.poi3:
		scene_poi3.queue_free()
		scene_poi3 = scene_poi3_helper.instance()
		return scene_poi3
	elif i == enums.positions.accurator:
		return scene_accurator
	elif i == enums.positions.trademill:
		return scene_trademill

func stop_time():
	print("Called stop time on Minigmae but We will not do anything about it")
	#Tick.pause()


func start(action):
	var rt = get_tree().get_nodes_in_group("RealTime")
	for i in rt:
		i.set_paused(true)
	state = true
	print("Minigame is: ",enums.positions.keys()[action])
	current_scene = actions_to_scenes(action)
	add_child(current_scene)
	move_child(current_scene,0)
	self.show()
	if (action == enums.positions.tv or action == enums.positions.table ):
		$minigame_tick.hide()
	else:
		$minigame_tick.show()
		$minigame_tick/presenter.start(5)
		if $minigame_tick/presenter.connect("finished",self,"timer_finished") != OK:
			push_warning("Could not connect")
		Tick.pause()
	if current_scene.has_method("init"): #meh line
		current_scene.init()
	current_action = action
	current_scene.connect("general_finished",self,"scene_finished")
	

#nearly same as scene_finished but it does it silently
#normaly finishing the popup ignites a goto from main
func force_quit():
	self.hide()
	for node in self.get_children():
		var signals = node.get_signal_list()
		for sig in signals:
			var connections = node.get_signal_connection_list(sig.name)
			for conn in connections:
				if node.is_connected(conn.signal,conn.target,conn.method) && node.get_name() != "minigame_tick":
					print("Connection:",conn.signal,conn.method)
					node.disconnect(conn.signal, conn.target, conn.method)
		if node.get_name() != "minigame_tick":
			remove_child(node)

func timer_finished(_fake_score):
	current_scene.force_quit()

func scene_finished(score):
	if current_action == enums.positions.tv:
		emit_signal("finished",current_action,0)
	else:
		emit_signal("finished",current_action,score)
		Tick.start()
	prefinished()

func prefinished():
	current_scene.disconnect("general_finished",self,"scene_finished")
	self.hide()
	remove_child(current_scene)
	if $minigame_tick/presenter.is_connected("finished",self,"timer_finished"):
		$minigame_tick/presenter.disconnect("finished",self,"timer_finished")
	else:
		print("minigmae is not connecting timee_finished")
	print("Finished with room ",enums.positions.keys()[current_action])
	var rt = get_tree().get_nodes_in_group("RealTime")
	for i in rt:
		i.set_paused(false)


func _process(_time):
	state= false


# Shop functions
func accurator_bought():
	emit_signal("accurator_bought")
	

func _ready():
	if !(scene_three_dee_printer.connect("accurator_bought",self,"accurator_bought") == OK):
		print("Could not connect with accurator_bought")

