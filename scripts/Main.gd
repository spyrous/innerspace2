extends Node2D

const enums = preload("common/enums.gd")
const parametrics = preload("common/parametrics.gd")

onready var bot_healthbar = $BotHealth
onready var route_bar = $Route
onready var fitness_bar = $Fitness
onready var bot = $Bot
onready var captain = $Captain
onready var depression =$Depression
onready var depression_array =$DepressionArray
onready var decks =	[$Deck0 ,$Deck1 ,$Deck2 ,$Deck3 ,$Deck4 ,$Deck5 ,$Deck6 ,$Deck7]
onready var energy_bar = $EnergyBar
onready var day_presenter = $global_day_presenter
onready var metal_bar = $MetalBar
onready var madrsPopup = $MadrsPopup
onready var popup = $Popup
onready var estimation = $estimation

var rng = RandomNumberGenerator.new() 
var current_depression_root 
var possible_rooms := [enums.positions.poi1,enums.positions.poi2,enums.positions.poi3,enums.positions.accurator,enums.positions.tv] #2 is three_dee_printer #4 is chill or eat # 7 is trademill
var day_ended :=false


#The rooms that are accessible now
func check_unlocked_rooms():
	#print("Checked unlocked room",energy_bar.get_score())
	if energy_bar.get_score() >= 50 &&  !(enums.positions.three_dee_printer in possible_rooms):
		decks[2].light()
		possible_rooms += [enums.positions.three_dee_printer]
	elif energy_bar.get_score() < 50 :
		decks[2].shade()
		possible_rooms.erase(enums.positions.three_dee_printer)


func aggregate_score(room,score):	
	if (room==enums.positions.poi1):
		print("Room at aggregate score is :",room,"and score is ",score,"metal")
		metal_bar.score(score)
	elif (room==enums.positions.poi2):  #bot health goes to bot health
		print("Room at aggregate score is :",room,"and score is ",score,"bot_healthbar")
		bot_healthbar.score(score)
	elif (room==enums.positions.poi3):
		print("Room at aggregate score is :",room,"and score is ",score,"energy bar")
		energy_bar.score(score)
	elif (room==enums.positions.accurator): 
		print("Room at aggregate score is :",room,"and score is ",score ,"route")
		route_bar.score(score)
	elif (room==enums.positions.trademill):
		print("Room at aggregate score is :",room,"and score is ",score,"fitness")
		fitness_bar.score(score)


func dispatch_event():
	print(current_depression_root.display())
	var type =  current_depression_root.get_action().get_type()
	match type :
		enums.action_t.cptn_display :
			captain.receive_depression_event(current_depression_root)
		enums.action_t.cptn_think :
			captain.receive_depression_event(current_depression_root)
		enums.action_t.stall:
			captain.waste_time()
			captain.disconnect("finished_a_move",popup,"start")
			captain.connect("finished_a_move",popup,"start",[4],CONNECT_ONESHOT)
		enums.action_t.cont:
			captain.gain_speed()
		enums.action_t.bot_display:
			bot.receive_conversation_event(current_depression_root)


func handle_captain_dialog_clicked():

	current_depression_root = current_depression_root.child(1)
	dispatch_event()


func handle_captain_dialog_missed():
	current_depression_root = current_depression_root.child(0)
	dispatch_event()


func handle_bot_dialog_clicked():
	current_depression_root = current_depression_root.child(1)
	dispatch_event()
	

func handle_bot_dialog_missed():
	current_depression_root = current_depression_root.child(0)
	dispatch_event()


func goto(prev_room,prev_score):
	#var prev_poi = Globals.current_poi
	#randomize_poi()
	#print("CAPTAIN GOES TO:",coords[Globals.current_poi]," But minigame is triggerd with room:",Globals.current_poi,"Current poi is :",Globals.current_poi)
	check_unlocked_rooms()
	$EventDecider.weighted_randomize_poi(possible_rooms)
	depression.decide_to_express_depression()

	captain.connect("finished_a_move",popup,"start",[Globals.current_poi],CONNECT_ONESHOT)
	captain.move($EventDecider.map_poi_to_coords(Globals.current_poi))
	aggregate_score(prev_room,prev_score)


func emmit_depression_event(event_number):
	current_depression_root = depression_array.get(event_number).get_root()
	dispatch_event()
	

func accurator_bought():
	metal_bar.update()


func handle_end_of_time():
	captain.sleep()
	bot.sleep()
	day_ended = true
	Tick.pause()
	popup.force_quit()
	for deck in decks:
		deck.shade()
	Globals.day = Globals.day + 1 
	day_presenter.update()

func new_day_or_madrs():
	if(Globals.day % 7 == 0) :
		madrsPopup.show()
		$MadrsPopup/MADR/Choices.grab_focus()
	else:
		new_day()	

func new_day():
	depression.new_day()
	$EventDecider.new_day()
	captain.wake_up()
	#popup.look_mirror()
	captain.gain_speed()
	Tick.restart(parametrics.max_minute * 60 + parametrics.max_second)
	check_unlocked_rooms()
	for deck in decks:
		if(deck != $Deck2 || enums.positions.three_dee_printer in possible_rooms):
			deck.light()
	Tick.start()
	goto(0,0)
	
func save_state():
	var file = File.new()
	file.open("user://save_state",File.WRITE)
	var nodes = get_tree().get_nodes_in_group("Persistant")
	for node in nodes:
		if node.filename.empty():
			print("persistent node '%s' is not an instanced scene, skipped" % node.name)
			continue
		var dict = node.save_state()
		file.store_line(to_json(dict))
	file.close()


func save_user_profiling_data():
	var file = File.new()
	file.open("user://save_user_profiling_data",File.WRITE)
	var nodes = get_tree().get_nodes_in_group("UserProfiling")
	for node in nodes:
		if node.filename.empty():
			print("User Profiling node '%s' is not an instanced scene, skipped" % node.name)
			continue
		var dict = node.save_user_profiling_data()
		file.store_line(to_json(dict))
	file.close()


func load_user_profiling_data():
	if(File.new().file_exists("user://save_user_profiling_data")):
		print("It exists!!!")
		var nodes = get_tree().get_nodes_in_group("UserProfiling")
		for node in nodes:
			if node.filename.empty():
				print("User profiling node '%s' is not an instanced scene, skipped" % node.name)
				continue
			node.load_user_profiling_data()
	else :
		print ("It doesn't exists")
		

func load_state():
	if(File.new().file_exists("user://save_state")):
		print("File save_state exists!!!")
		var nodes = get_tree().get_nodes_in_group("Persistant")
		for node in nodes:
			if node.filename.empty():
				print("persistent node '%s' is not an instanced scene, skipped" % node.name)
				continue
			node.load_state()
	else :
		print ("File save_state doesn't exist")



func _input(event):
#	if event.is_action_pressed("ui_focus_next") && !day_ended:
#		madrsPopup.hide()
#	elif event.is_action_pressed("ui_focus_next") && !day_ended:
#		madrsPopup.show()
#		$MadrsPopup/MADR/Choices.grab_focus()
	if event.is_action_pressed("ui_focus_next") && day_ended:
		day_ended= false
		madrsPopup.hide()
		Globals.update_estimations()
		save_user_profiling_data()
		new_day()
		estimation.add_week()
		if !( get_tree().change_scene("res://scenes/ui/finished.tscn") == OK):
			print("Coudn't nor change_scene to ui/start")
	elif  event.is_action_pressed("Escape") && !madrsPopup.visible && !popup.is_visible():
		save_state()
		Tick.pause()
		if !( get_tree().change_scene("res://scenes/ui/start.tscn") == OK):
			print("Coudn't nor change_scene to ui/start")

		captain.disconnect_signals()


func _ready():

	Tick.start()
	load_state()
	if Globals.day == 0 :
		Globals.init()
	day_presenter.update()
	check_unlocked_rooms()
	metal_bar.init(1)
	bot_healthbar.init(2)
	energy_bar.init(3)
	route_bar.init(4)
	fitness_bar.init(5)
	rng.randomize()
	depression.connect("depression_event",self,"emmit_depression_event")
	popup.connect("finished",self,"goto")
	#goto(0,0)
	new_day()
	popup.connect("accurator_bought",self,"accurator_bought")
	captain.connect("dialog_clicked",self,"handle_captain_dialog_clicked")
	captain.connect("dialog_missed",self,"handle_captain_dialog_missed")
	bot.connect("dialog_clicked",self,"handle_bot_dialog_clicked")
	bot.connect("dialog_missed",self,"handle_bot_dialog_missed")

	if captain.connect("sleeping",$global_tick,"express_sleep_reduction") != OK:
		push_warning("Couldn't connect captain finished_move_with sleep")
	if $global_tick.connect("light_up",decks[0],"light") != OK:
		push_warning("Could not connect")
	if $global_tick.connect("light_down",decks[0],"shade") != OK:
		push_warning("Could not connect")
	if $global_tick.connect("night_finished",self,"new_day_or_madrs") != OK:
		push_warning("Could not connect")
	if !(Tick.connect("finished",self,"handle_end_of_time") == OK):
		print("Coudn't connect tick.finished with handle_end_of_time")

